import { AuthState, AuthAction } from './systems/auth/auth.duck'
import { NavigationState, NavigationAction } from './systems//navigation/navigation'
import { TeasState, TeasAction } from './systems/teas/teas.duck'
import { TeaCategoriesState, TeaCategoriesAction } from './systems//teaCategories/teaCategories'
import { ThunkAction } from 'redux-thunk'
import { RouterAction } from 'connected-react-router'

export interface RootState {
  navigation: NavigationState
  teas: TeasState
  teaCategories: TeaCategoriesState
  auth: AuthState,
}

export type AllActions =
  | NavigationAction
  | TeasAction
  | TeaCategoriesAction
  | AuthAction
  | RouterAction

export type TeazThunk = ThunkAction<Promise<void>, RootState, any, AllActions>
