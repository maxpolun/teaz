import { createStore, applyMiddleware, compose, Middleware } from 'redux'
import thunk from 'redux-thunk'
import reducer from './reducer'
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'

export const history = createBrowserHistory()

const middleware: Middleware[] = [
  thunk,
  routerMiddleware(history)
]
const enhancers = [
  applyMiddleware(...middleware)
]

const composeEnhancers = ((window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose)
const store = createStore(
  connectRouter(history)(reducer),
  composeEnhancers(...enhancers))
if ((module as any).hot) {
  (module as any).hot.accept('./reducer', () => {
    console.log('updating reducers')
    const nextReducers = require('./reducer')
    store.replaceReducer(connectRouter(history)(nextReducers))
  })
}

export default store
