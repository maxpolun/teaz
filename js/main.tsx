import * as React from 'react'
import { render } from 'react-dom'
import App from './App'
import LandingPage from './pages/LandingPage'

if (process.env.WEBPACK_SERVE) {
  require('webpack-serve-overlay')
}

render(<App><LandingPage /></App>, document.getElementById('main'))
