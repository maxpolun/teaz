import { combineReducers } from 'redux'
import { navigationReducer as navigation } from './systems//navigation/navigation'
import { teasReducer as teas } from './systems/teas/teas.duck'
import { teaCategoriesReducer as teaCategories } from './systems//teaCategories/teaCategories'
import { authReducer as auth } from './systems/auth/auth.duck'
import { reducer as form } from 'redux-form'

export default combineReducers({
  navigation,
  teas,
  teaCategories,
  auth,
  form
})
