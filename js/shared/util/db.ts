export type Db<T> = {[id: string]: T}

export class DbModule<T> {
  constructor (private getKey: (obj: T) => string) {}

  fromArray (arr: T[]): Db<T> {
    let db: Db<T> = {}
    arr.forEach(item => db[this.getKey(item)] = item)
    return db
  }

  add (db: Db<T>, item: T): Db<T> {
    return Object.assign({}, db, { [this.getKey(item)]: item })
  }

  toArray (db: Db<T>): T[] {
    return Object.keys(db).map(key => db[key])
  }
}
