export const get = (path: string): Promise<any> => fetch(path, {
  credentials: 'same-origin',
  headers: {
    'Accept': 'application/json'
  }
}).then(r => {
  if (!r.ok) throw r
  return r.json()
})

export const post = (path: string, body: any): Promise<any> => fetch(path, {
  method: 'POST',
  credentials: 'same-origin',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(body)
}).then(r => {
  if (!r.ok) throw r
  return r.json()
})
