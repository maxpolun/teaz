import Layout from './Layout'
import * as React from 'react'
import { shallow } from 'enzyme'

describe('Layout', () => {
  function render () {
    return shallow(<Layout showNav toggleNav={() => ({})}><span>test</span></Layout>)
  }
  it('can render', () => {
    expect(render()).toExist()
  })
})
