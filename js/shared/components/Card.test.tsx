import Card from './Card'
import * as React from 'react'
import { shallow } from 'enzyme'

describe('Card', () => {
  function render () {
    return shallow(<Card header='Test Card'>Card Body</Card>)
  }
  it('can render', () => {
    expect(render()).toExist()
  })
})
