import * as React from 'react'

interface CardDeckProps {
  children: React.ReactNode
}

const CardDeck: React.SFC<CardDeckProps> = ({ children }) => <div className='card-deck'>{children}</div>

export default CardDeck
