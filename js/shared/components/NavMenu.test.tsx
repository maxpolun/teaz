import NavMenu from './NavMenu'
import * as React from 'react'
import {shallow} from 'enzyme'

describe('NavMenu', () => {
  function render () {
    return shallow(<NavMenu />)
  }
  it('can render', () => {
    expect(render()).toExist()
  })
})
