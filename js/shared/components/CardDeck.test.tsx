import CardDeck from './CardDeck'
import * as React from 'react'
import { shallow } from 'enzyme'

describe('CardDeck', () => {
  function render () {
    return shallow(<CardDeck>child</CardDeck>)
  }
  it('can render', () => {
    expect(render()).toExist()
  })
})
