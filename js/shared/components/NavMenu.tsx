import * as React from 'react'
import { NavLink } from 'react-router-dom'
import LogoutBtn from '@src/systems/auth/LogoutBtn.container'

interface NavMenuProps {

}

export default class NavMenu extends React.Component<NavMenuProps> {
  render () {
    return <>
      <NavLink to='/' activeClassName='active' exact>Dashboard</NavLink>
      <NavLink to='/teas' activeClassName='active'>Teas</NavLink>
      <NavLink to='/teaware' activeClassName='active'>Teaware</NavLink>
      <NavLink to='/sessions' activeClassName='active'>Sessions</NavLink>
      <LogoutBtn />
    </>
  }
}
