import StarRatingPicker from './StarRatingPicker'
import * as React from 'react'
import { shallow } from 'enzyme'

describe('StarRatingPicker', () => {
  function render () {
    return shallow(<StarRatingPicker
      onChange={() => { return }}
      name='test'
    />)
  }
  it('can render', () => {
    expect(render()).toExist()
  })
})
