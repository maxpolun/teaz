import * as React from 'react'
import { WrappedFieldProps, Field, GenericField } from 'redux-form'
import './FormControl.scss'

interface FormControlCustomProps {
  type: string,
  className?: string,
  label: React.ReactNode,
}

export const FormControlField = Field as new () => GenericField<FormControlCustomProps>

type FormControlProps = FormControlCustomProps & WrappedFieldProps

export default class FormControl extends React.Component<FormControlProps> {
  render () {
    const { type, className = '', label, input, meta } = this.props
    const fieldName = `${input.name}-${meta.form}`
    const fieldClass = `${type}-field ${className}`
    const inputElem = type === 'text'
      ? <input id={fieldName} name={fieldName} className={fieldClass} {...input} />
      : <textarea id={fieldName} name={fieldName} className={fieldClass} {...input} />
    return <div className='field-group'>
      <label htmlFor={fieldName}>{label}</label>
      {inputElem}
      { meta.error ? <span className='form-error'>{meta.error}</span> : null}
    </div>
  }
}
