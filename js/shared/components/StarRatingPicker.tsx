import * as React from 'react'
import { FiStar } from 'react-icons/fi'
import './StarRatingPicker.scss'

interface StarRatingPickerProps {
  max: number,
  value?: string,
  onChange: React.ChangeEventHandler<HTMLInputElement>,
  name: string,

}

function range (min: number, max: number) {
  return [...new Array(max - min + 1).keys()].map(i => i + min)
}

export default class StarRatingPicker extends React.Component<StarRatingPickerProps> {
  static defaultProps = {
    max: 5
  }

  render () {
    const { max, value = '0', onChange, name } = this.props
    const val = parseInt(value, 10)
    return <div className='star-picker'>
      {range(1, max).map(i => {
        const checked = !!value && i <= val
        return <label key={i}><input
            className='star-radio'
            type='radio'
            value={i}
            checked={i === val}
            onChange={onChange}
            name={name}
          /><span className='sr-only'>{i}</span>
          <FiStar className={`star-picker-${checked ? 'checked' : 'unchecked'}`} />
        </label>
      })}
    </div>
  }
}
