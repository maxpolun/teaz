import * as React from 'react'
import { WrappedFieldProps, Field, GenericField } from 'redux-form'
import StarRatingPicker from './StarRatingPicker'

interface StarRatingFieldCustomProps {
  className?: string,
  label: React.ReactNode,
}

export const StarRatingField = Field as new () => GenericField<StarRatingFieldCustomProps>

type StarRatingFieldProps = StarRatingFieldCustomProps & WrappedFieldProps

export default class StarRatingControl extends React.Component<StarRatingFieldProps> {
  render () {
    const { className = '', label, input, meta } = this.props
    const fieldName = `${input.name}-${meta.form}`
    const fieldClass = `$star-rating-field ${className}`
    return <div className='field-group'>
      <label htmlFor={fieldName}>{label}</label>
      <StarRatingPicker {...input} />
      { meta.error ? <span className='form-error'>{meta.error}</span> : null}
    </div>
  }
}
