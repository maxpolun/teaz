import * as React from 'react'
import './Card.scss'

interface CardProps {
  header: React.ReactNode,
  children: React.ReactNode
}
interface CardState {
}

export default class Card extends React.Component<CardProps, CardState> {
  render () {
    const { children, header } = this.props
    return <div className='card'>
      <div className='card-header'>
        <h3>{header}</h3>
      </div>
      <div className='card-body'>
        {children}
      </div>
    </div>
  }
}
