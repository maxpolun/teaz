import * as React from 'react'
import './Layout.scss'
import { FiMenu } from 'react-icons/fi'

interface LayoutProps {
  children: JSX.Element,
  nav?: JSX.Element
  showNav: boolean,
  toggleNav: () => any
}

export class Layout extends React.Component<LayoutProps> {
  render () {
    return <div className='page-layout'>
      <header className='page-header'>
      <h1>
      <button type='button' className='btn-link mobile-only' onClick={this.props.toggleNav}>
        <FiMenu />
        <span className='sr-only'>Open Menu</span>
      </button>
        Teaz
      </h1>
        {this.props.nav ? <nav className={`page-navigation ${this.props.showNav && 'show'}`}>{this.props.nav}</nav> : null }
        {this.props.nav && this.props.showNav ? <div className='nav-overlay mobile-only' onClick={this.props.toggleNav}/> : null }
      </header>
      <div className='page-body'>
        {this.props.children}
      </div>
    </div>
  }
}

export default Layout
