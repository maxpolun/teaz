import { AllActions, RootState } from '@src/reduxTypes'
import { AnyAction } from 'redux'

export interface NavigationState {
  open: boolean
}

const defaultState: NavigationState = {
  open: false
}

// hygen:actionTypes
export const NAV_TOGGLED = 'teaz/navigation/navToggled'
export type NAV_TOGGLED = 'teaz/navigation/navToggled'

export const NAV_CLOSED = 'teaz/navigation/navClosed'
export type NAV_CLOSED = 'teaz/navigation/navClosed'

export const NAV_OPENED = 'teaz/navigation/navOpened'
export type NAV_OPENED = 'teaz/navigation/navOpened'

// hygen:actionCreators
export interface NavToggled {
  type: NAV_TOGGLED
}
export const navToggled = (): NavToggled => ({ type: NAV_TOGGLED })

export interface NavClosed {
  type: NAV_CLOSED
}
export const navClosed = (): NavClosed => ({ type: NAV_CLOSED })

export interface NavOpened {
  type: NAV_OPENED
}
export const navOpened = (): NavOpened => ({ type: NAV_OPENED })

export type NavigationAction =
  | NavToggled
  | NavClosed
  | NavOpened

export function navigationReducer (state: NavigationState = defaultState, action: AllActions): NavigationState {
  switch (action.type) {
    case NAV_OPENED: return { open: true }
    case NAV_CLOSED: return { open: false }
    case NAV_TOGGLED: return { open: !state.open }
    default: return state
  }
}

export const navigationSelector = (state: RootState) => state.navigation
