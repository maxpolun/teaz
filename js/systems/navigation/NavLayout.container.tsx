import * as React from 'react'
import { connect } from 'react-redux'
import { navigationSelector, navToggled } from './navigation'
import Layout from '@src/shared/components/Layout'

interface NavLayoutProps {
  children: JSX.Element,
  nav?: JSX.Element,
  open: boolean,
  navToggled: typeof navToggled
}

export const NavLayout: React.SFC<NavLayoutProps> = (props) => {
  return <Layout nav={props.nav} showNav={props.open} toggleNav={props.navToggled}>{props.children}</Layout>
}

export default connect(navigationSelector, { navToggled })(NavLayout)
