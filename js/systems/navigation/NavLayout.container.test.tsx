import * as React from 'react'
import { NavLayout } from './NavLayout.container'
import { shallow } from 'enzyme'
import { navToggled } from './navigation'

describe('NavLayout', () => {
  function render () {
    return shallow(<NavLayout open navToggled={navToggled}><span /></NavLayout>)
  }
  it('can render', () => {
    expect(render()).toExist()
  })
})
