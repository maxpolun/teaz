import TeaCategoryField from './TeaCategoryField'
import * as React from 'react'
import { shallow } from 'enzyme'

const nullfn = (): any => { return }

describe('TeaCategoryField', () => {
  function render () {
    return shallow(<TeaCategoryField
      input={{ value: 'test', onBlur: nullfn, onChange: nullfn, name: 'test', onDragStart: nullfn, onDrop: nullfn, onFocus: nullfn }}
      meta={{ autofilled: false, asyncValidating: false, dirty: false, touched: false, dispatch: nullfn, form: 'test', initial: '', invalid: false, pristine: true, submitting: false, submitFailed: false, valid: true, visited: false }}
    />)
  }
  it('can render', () => {
    expect(render()).toExist()
  })
})
