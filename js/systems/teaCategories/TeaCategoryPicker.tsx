import * as React from 'react'
import { TeaCategory } from './teaCategory.model'
import { connect } from 'react-redux'
import { teaCategoriesListSelector } from './teaCategories'
import { RootState } from '@src/reduxTypes'

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

interface TeaCategoryPickerProps extends Omit<React.SelectHTMLAttributes<HTMLSelectElement>, 'onChange' | 'value'> {
  categories: TeaCategory[]
  onChange: (category: TeaCategory) => void,
  value?: TeaCategory
}

export class TeaCategoryPicker extends React.Component<TeaCategoryPickerProps> {
  handleSelect = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const cat = this.props.categories.find(c => c.id.toString() === event.target.value)
    if (!cat) throw new Error('missing tea category when selecting')
    this.props.onChange(cat)
  }

  render () {
    const { categories, value, onChange, ...attrs } = this.props
    return <select onChange={this.handleSelect} value={value ? value.id : undefined} {... attrs }>
      { categories.map(c => <option key={c.id} value={c.id}>{c.name}</option>) }
    </select >
  }
}

export default connect((state: RootState) => ({ categories: teaCategoriesListSelector(state) }))(TeaCategoryPicker)
