import { TeaCategoryPicker } from './TeaCategoryPicker'
import * as React from 'react'
import { shallow } from 'enzyme'

describe('TeaCategoryPicker', () => {
  function render () {
    return shallow(<TeaCategoryPicker categories={[]} onChange={() => { return }} />)
  }
  it('can render', () => {
    expect(render()).toExist()
  })
})
