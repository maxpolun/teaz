import { AllActions, RootState, TeazThunk } from '@src/reduxTypes'
import { TeaCategory, getTeaCategories } from './teaCategory.model'
import { createSelector } from 'reselect'
import { Dispatch } from 'redux'

export interface TeaCategoriesState {
  [id: string]: TeaCategory
}

const defaultState: TeaCategoriesState = {}

// hygen:actionTypes
export const TEA_CATEGORIES_LOADED = 'teaz/teaCategories/teaCategoriesLoaded'
export type TEA_CATEGORIES_LOADED = 'teaz/teaCategories/teaCategoriesLoaded'

// hygen:actionCreators
export interface TeaCategoriesLoaded {
  type: TEA_CATEGORIES_LOADED,
  payload: TeaCategory[]
}
export const teaCategoriesLoaded = (categories: TeaCategory[]): TeaCategoriesLoaded => ({ type: TEA_CATEGORIES_LOADED, payload: categories })

export type TeaCategoriesAction =
  | TeaCategoriesLoaded

export function teaCategoriesReducer (state: TeaCategoriesState = defaultState, action: AllActions): TeaCategoriesState {
  switch (action.type) {
    case TEA_CATEGORIES_LOADED: return action.payload.reduce((cats: TeaCategoriesState, cat: TeaCategory) => {
      cats[cat.id] = cat
      return cats
    }, {})
    default: return state
  }
}

export const teaCategoriesSelector = (state: RootState) => state.teaCategories
export const teaCategoriesListSelector = createSelector(teaCategoriesSelector, cats => (
  Object.keys(cats).map(id => cats[id])
))

const isEmptyObject = (o: any): boolean => !Object.getOwnPropertyNames(o).length

export const ensureTeaCategoriesLoaded = (): TeazThunk => async (dispatch: Dispatch<AllActions>, getState: () => RootState) => {
  const teaCategories = teaCategoriesSelector(getState())
  if (isEmptyObject(teaCategories)) {
    dispatch(teaCategoriesLoaded(await getTeaCategories()))
  }
}
