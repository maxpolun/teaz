import { get } from '@src/shared/util/http'
import { API_BASE } from '@src/config'

export interface TeaCategory {
  id: number,
  name: string
}

export const getTeaCategories = (): Promise<TeaCategory[]> => get(API_BASE + '/tea-categories')
