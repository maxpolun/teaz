import * as React from 'react'
import { WrappedFieldProps } from 'redux-form'
import TeaCategoryPicker from './TeaCategoryPicker'

export default class TeaCategoryField extends React.Component<WrappedFieldProps> {
  render () {
    const { input, meta } = this.props
    const fieldName = `${input.name}-${meta.form}`
    return <div className='field-group'>
      <label htmlFor={fieldName}>Tea Category</label>
      <TeaCategoryPicker id={fieldName} name={fieldName} className='select-field tea-category-picker' {...input} />
      { meta.error ? <span className='form-error'>{meta.error}</span> : null}
    </div>
  }
}
