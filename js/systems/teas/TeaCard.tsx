import * as React from 'react'
import Card from '@src/shared/components/Card'
import { Tea } from './tea.model'

interface TeaCardProps {
  tea: Tea
}
interface TeaCardState {
}

export default class TeaCard extends React.Component<TeaCardProps, TeaCardState> {
  render () {
    const { tea } = this.props
    return <Card header={tea.name}>
      {tea.description != null && <p>{tea.description}</p>}
      <p>{tea.category.name} {tea.subcategory}</p>
      {tea.rating != null && <p>Rating: {tea.rating}</p>}
    </Card>
  }
}
