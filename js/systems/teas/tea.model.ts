import { TeaCategory } from '@src/systems/teaCategories/teaCategory.model'
import { get, post } from '@src/shared/util/http'
import { API_BASE } from '@src/config'

export interface NewTea {
  category: TeaCategory,
  subcategory?: string,
  name: string,
  description?: string,
  rating?: number,
  url?: string
}

export interface Tea extends NewTea {
  id: string,
}

const TEAS_PATH = API_BASE + '/users/me/teas'

export const getTeas = (): Promise<Tea[]> => get(TEAS_PATH)
export const createTea = (tea: NewTea): Promise<Tea> => {
  const { category, ...otherFields } = tea
  return post(TEAS_PATH, { category_id: category.id, ...otherFields })
}
