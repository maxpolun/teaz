import * as React from 'react'
import { connect } from 'react-redux'
import { teaListSelector, loadTeas } from '@src/systems/teas/teas.duck'
import { Tea } from '@src/systems/teas/tea.model'
import CardDeck from '@src/shared/components/CardDeck'
import TeaCard from './TeaCard'

interface TeaListProps {
  teas: Tea[],
  loading: boolean,
  loaded: boolean,
  loadTeas: typeof loadTeas
}

export class TeaList extends React.Component<TeaListProps> {
  render () {
    return <CardDeck>
      {this.props.teas.map(t => <TeaCard key={t.id} tea={t} />)}
    </CardDeck>
  }

  componentDidMount () {
    this.props.loadTeas()
  }
}

export default connect(teaListSelector, { loadTeas })(TeaList)
