import * as React from 'react'
import { TeaList } from './TeaList'
import { shallow } from 'enzyme'

describe('TeaList', () => {
  let loadTeas: () => any
  function render () {
    return shallow(<TeaList teas={[]} loading={false} loaded loadTeas={loadTeas}/>)
  }

  beforeEach(() => {
    loadTeas = jest.fn()
  })
  it('can render', () => {
    expect(render()).toExist()
  })
})
