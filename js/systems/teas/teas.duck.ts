import { AllActions, RootState } from '@src/reduxTypes'
import { AnyAction, Dispatch } from 'redux'
import { Tea, getTeas } from '@src/systems/teas/tea.model'
import { DbModule } from '@src/shared/util/db'
import { createSelector } from 'reselect'
import { ThunkAction } from 'redux-thunk'

const TeaDb = new DbModule<Tea>(tea => tea.id)

export interface TeasState {
  loading: boolean,
  loaded: boolean,
  teas: {[id: string]: Tea}
}

const defaultState: TeasState = {
  loading: false,
  loaded: false,
  teas: {}
}

// hygen:actionTypes
export const TEAS_LOADING = 'teaz/teas/teasLoading'
export const TEA_ADDED = 'teaz/teas/teaAdded'
export const TEAS_LOADED = 'teaz/teas/teasLoaded'

// hygen:actionCreators
export interface TeasLoading {
  type: typeof TEAS_LOADING
}
export const teasLoading = (): TeasLoading => ({ type: TEAS_LOADING })

export interface TeaAdded {
  type: typeof TEA_ADDED,
  payload: Tea
}
export const teaAdded = (tea: Tea): TeaAdded => ({ type: TEA_ADDED, payload: tea })

export interface TeasLoaded {
  type: typeof TEAS_LOADED,
  payload: Tea[]
}
export const teasLoaded = (teas: Tea[]): TeasLoaded => ({ type: TEAS_LOADED, payload: teas })

export type TeasAction =
  | TeasLoading
  | TeaAdded
  | TeasLoaded

export function teasReducer (state: TeasState = defaultState, action: AllActions): TeasState {
  switch (action.type) {
    case TEAS_LOADING: return Object.assign({}, state, { loading: true })
    case TEAS_LOADED: return Object.assign({}, state, { loading: false, loaded: true, teas: TeaDb.fromArray(action.payload) })
    case TEA_ADDED: return Object.assign({}, state, { teas: TeaDb.add(state.teas, action.payload) })
    default: return state
  }
}

export const loadTeas: () => ThunkAction<Promise<void>, RootState, any, AllActions> = () => async (dispatch: Dispatch<AllActions>) => {
  dispatch(teasLoading())
  dispatch(teasLoaded(await getTeas()))
}

export const teasSelector = (state: RootState) => state.teas
export const teaListSelector = createSelector(teasSelector, ({ teas, loading, loaded }) => ({ teas: TeaDb.toArray(teas), loading, loaded }))
