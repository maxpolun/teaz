import * as React from 'react'
import { reduxForm, InjectedFormProps, Field, FormErrors } from 'redux-form'
import { Dispatch } from 'redux'
import { AllActions } from '@src/reduxTypes'
import { push } from 'connected-react-router'
import { createTea, NewTea } from './tea.model'
import FormControl, { FormControlField } from '@src/shared/components/FormControl'
import { teaAdded } from './teas.duck'
import TeaCategoryField from '../teaCategories/TeaCategoryField'
import StarRatingControl, { StarRatingField } from '@src/shared/components/StarRatingControl'

type NewTeaProps = InjectedFormProps<NewTea>
export const NewTeaForm: React.SFC<NewTeaProps> = (props: NewTeaProps) => {
  return <form className='new-tea' onSubmit={props.handleSubmit}>
    <h2>Create New Tea</h2>
    {
      props.error ? <div className='alert error'>
          There was an error adding the tea
        </div> : null
    }
    <FormControlField
      component={FormControl}
      type='text'
      name='name'
      label='Name'
    />
    <FormControlField
      component={FormControl}
      type='textarea'
      name='description'
      label='Description'
    />
    <Field
      component={TeaCategoryField}
      name='category'
      onBlur={e => e && e.preventDefault()}
    />
    <FormControlField
      component={FormControl}
      type='text'
      name='subcategory'
      label='Sub Category'
    />
    <StarRatingField
      component={StarRatingControl}
      name='rating'
      label='Rating'
    />
    <FormControlField
      component={FormControl}
      type='text'
      name='url'
      label='Store URL'
    />
    <button className='btn primary'>
      Submit
    </button>
  </form >
}

export default reduxForm<NewTea>({
  form: 'new-tea',
  onSubmit: async (values, dispatch: Dispatch<AllActions>) => {
    const tea = await createTea(values)
    dispatch(teaAdded(tea))
    dispatch(push('/teas'))
  },
  validate: (fields) => {
    let errors: FormErrors<NewTea, string> = {}
    if (!fields.name || fields.name.trim() === '') {
      errors.name = 'Name is required'
    }
    if (!fields.category) {
      errors.category = 'You must pick a category'
    }
    return errors
  }
})(NewTeaForm)
