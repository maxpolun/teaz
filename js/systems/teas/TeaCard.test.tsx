import TeaCard from './TeaCard'
import * as React from 'react'
import { shallow } from 'enzyme'

describe('TeaCard', () => {
  function render () {
    return shallow(<TeaCard tea={{ id: 'x', category: { id: 1, name: 'black' }, name: 'black tea' }} />)
  }
  it('can render', () => {
    expect(render()).toExist()
  })
})
