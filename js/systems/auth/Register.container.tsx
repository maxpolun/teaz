import * as React from 'react'
import { Link } from 'react-router-dom'
import { reduxForm, InjectedFormProps, Field, SubmissionError } from 'redux-form'
import { push } from 'connected-react-router'
import { register, RegisterError, RegisterErrorType } from '@src/systems/auth/login'

interface RegisterFormFields {
  email: string,
  password: string,
  password_confirmation: string
}
type RegisterProps = InjectedFormProps<RegisterFormFields>

class Register extends React.Component<RegisterProps> {
  render () {
    return <form className='register' onSubmit={this.props.handleSubmit}>
      <h2>Register</h2>
      {
        this.props.error ? <div className='alert error'>
          {this.props.error}
        </div> : null
      }
      <div className='field-group'>
        <label htmlFor='email'>Email:</label>
        <Field
          type='text'
          className='text-field'
          name='email'
          component='input'
        />
      </div>

      <div className='field-group'>
        <label htmlFor='password'>Password:</label>
        <Field
          type='password'
          className='text-field'
          name='password'
          component='input'
        />
      </div>

      <div className='field-group'>
        <label htmlFor='password_confirmation'>Password Confirmation:</label>
        <Field
          type='password'
          className='text-field'
          name='password_confirmation'
          component='input'
        />
      </div>
      <button
        className='btn primary'
      >
        Submit
      </button>
      <Link to='/login'>Login to existing account</Link>
    </form>
  }
}

export default reduxForm<RegisterFormFields>({
  form: 'register',
  onSubmit: async (values, dispatch) => {
    try {
      await register({
        email: values.email,
        password: values.password,
        password_confirmation: values.password_confirmation
      })
      dispatch(push('/login'))
    } catch (e) {
      if (e instanceof RegisterError) {
        switch (e.type) {
          case RegisterErrorType.InvalidEmail: throw new SubmissionError({ email: 'invalid', _error: 'Invalid Email' })
          case RegisterErrorType.InvalidPassword: throw new SubmissionError({ password: 'invalid', _error: 'Invalid Password' })
          case RegisterErrorType.OtherError: throw new SubmissionError({ _error: 'An error occurred' })
          case RegisterErrorType.PasswordConfirmationNoMatch: throw new SubmissionError({ _error: 'password and confirmation must match' })
          case RegisterErrorType.UserExists: throw new SubmissionError({ _error: 'A user already exists with that email' })
        }
      } else {
        throw new SubmissionError({ _error: 'An error occurred' })
      }
    }
  }
})(Register)
