import { API_BASE } from '@src/config'
import { post } from '@src/shared/util/http'

export interface LoginRequestBody {
  email: string,
  password: string
}

export interface User {
  id: string,
  email: string,
}

export async function login (request: LoginRequestBody): Promise<User> {
  const response = await fetch(`${API_BASE}/login`, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(request)
  })

  if (!response.ok) {
    throw new Error('unable to login')
  }
  return response.json()
}

export interface RegisterRequestBody {
  email: string,
  password: string,
  password_confirmation: string
}
export enum RegisterErrorType {
  InvalidPassword = 'invalid_password',
  PasswordConfirmationNoMatch = 'password_confirmation_no_match',
  InvalidEmail = 'invalid_email',
  UserExists = 'user_exists',
  OtherError = 'other_error'
}

export class RegisterError extends Error {
  constructor (public type: RegisterErrorType) {
    super()
  }
}

export async function register (request: RegisterRequestBody): Promise<User> {
  const response = await fetch(`${API_BASE}/register`, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(request)
  })

  if (!response.ok) {
    let body = await response.json()
    throw new RegisterError(body.error)
  }
  return response.json()
}

export async function logout (): Promise <void> {
  await post(`${API_BASE}/logout`, '')
}
