import * as React from 'react'
import { Link } from 'react-router-dom'
import { reduxForm, InjectedFormProps, Field } from 'redux-form'
import { login } from '@src/systems/auth/login'
import { Dispatch } from 'redux'
import { AllActions } from '@src/reduxTypes'
import { authAdded } from '@src/systems/auth/auth.duck'
import { push } from 'connected-react-router'

interface LoginFormFields {
  email: string,
  password: string
}

type LoginProps = InjectedFormProps<LoginFormFields>

export const Login: React.SFC<LoginProps> = (props: LoginProps) => {
  return <form className='login' onSubmit={props.handleSubmit}>
      <h2>Login</h2>
    {
      props.error ? <div className='alert error'>
          There was an error logging in, please try again
        </div> : null
    }
    <div className='field-group'>
        <label htmlFor='email'>Email:</label>
        <Field
          component='input'
          type='text'
          className='text-field'
          name='email'
        />
      </div>

      <div className='field-group'>
        <label htmlFor='password'>Password:</label>
        <Field
          component='input'
          type='password'
          className='text-field'
          name='password'
        />
      </div>
      <button
        className='btn primary'
      >
        Submit
      </button>
      <Link to='/register'>I do not have an account</Link>
    </form >
}

export default reduxForm<LoginFormFields>({
  form: 'login',
  onSubmit: async (values, dispatch: Dispatch<AllActions>) => {
    let user = await login(values)
    dispatch(authAdded(user))
    push('/')
  }
})(Login)
