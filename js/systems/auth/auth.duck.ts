import { User, logout } from '@src/systems/auth/login'
import { AllActions, RootState } from '../../reduxTypes'
import { createSelector } from 'reselect'
import { ThunkAction } from 'redux-thunk'
import { Dispatch } from 'redux'
import getUser from '@src/systems/getUser'

export interface AuthState {
  user: User | null,
  loaded: boolean,
  loading: boolean
}

const defaultState: AuthState = {
  user: null,
  loaded: false,
  loading: false
}

const AUTH_ADDED = 'teaz/auth/add'
export interface AddAuthAction {
  type: typeof AUTH_ADDED,
  payload: User
}
export const authAdded = (u: User): AddAuthAction => ({ type: AUTH_ADDED, payload: u })

const AUTH_REMOVED = 'teaz/auth/remove'
export interface RemoveAuthAction {
  type: typeof AUTH_REMOVED
}
export const authRemoved = (): RemoveAuthAction => ({ type: AUTH_REMOVED })

const AUTH_LOADED = 'teaz/auth/loading'
export interface AuthLoadedAction {
  type: typeof AUTH_LOADED
}
export const authLoading = (): AuthLoadedAction => ({ type: AUTH_LOADED })

export type AuthAction = AddAuthAction | RemoveAuthAction | AuthLoadedAction

export function authReducer (state: AuthState = defaultState, action: AllActions): AuthState {
  switch (action.type) {
    case AUTH_ADDED: return { user: action.payload, loading: false, loaded: true }
    case AUTH_REMOVED: return defaultState
    default: return state
  }
}

export const loadAuth: () => ThunkAction<Promise<void>, RootState, any, AllActions> = () => async (dispatch: Dispatch<AllActions>) => {
  dispatch(authLoading())
  const user = await getUser()
  dispatch(authAdded(user))
}

export const logoutAuth: () => ThunkAction<Promise<void>, RootState, any, AllActions> = () => async (dispatch: Dispatch<AllActions>) => {
  await logout()
  dispatch(authRemoved())
}

export const authSelector = (state: RootState) => state.auth
export const currentUser = createSelector(authReducer, auth => auth.user)
export const isLoggedIn = createSelector(currentUser, user => !!user)
