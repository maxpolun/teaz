import * as React from 'react'
import {LogoutBtn} from './LogoutBtn.container'
import {shallow} from 'enzyme'

describe('LogoutBtn', () => {
  function render () {
    return shallow(<LogoutBtn />)
  }
  it('can render', () => {
    expect(render()).toExist()
  })
})
