import * as React from 'react'
import { connect } from 'react-redux'
import { logoutAuth } from './auth.duck'

interface LogoutBtnProps {
  logoutAuth: typeof logoutAuth
}

export const LogoutBtn: React.SFC<LogoutBtnProps> = ({ logoutAuth }) => {
  return <button
    type='button'
    className='btn logout'
    onClick={logoutAuth}
  >Logout</button>
}

export default connect(state => ({}), { logoutAuth })(LogoutBtn)
