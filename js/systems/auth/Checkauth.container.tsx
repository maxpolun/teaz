import * as React from 'react'
import { AuthState, authSelector, loadAuth } from './auth.duck'
import { connect } from 'react-redux'

interface CheckauthProps extends AuthState {
  authorized: JSX.Element,
  unauthorized: JSX.Element,
  loadAuth: typeof loadAuth
}

export class Checkauth extends React.Component<CheckauthProps> {
  render () {
    const { user, authorized, unauthorized } = this.props
    if (user !== null) return authorized
    return unauthorized
  }

  componentDidMount () {
    this.props.loadAuth()
  }
}

export default connect(authSelector, { loadAuth })(Checkauth)
