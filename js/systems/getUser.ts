import { get } from '@src/shared/util/http'
import { API_BASE } from '@src/config'
import { User } from '@src/systems/auth/login'

export default (): Promise<User> => get(API_BASE + '/users/me')
