import * as React from 'react'
import { Provider } from 'react-redux'
import { default as store, history } from './store'
import { ConnectedRouter } from 'connected-react-router'
import './App.scss'

export interface AppProps {
  children: JSX.Element | string
}

export default class App extends React.Component<AppProps> {
  render () {
    return <Provider store={store}>
      <ConnectedRouter history={history}>
        {this.props.children}
      </ConnectedRouter>
    </Provider>
  }
}
