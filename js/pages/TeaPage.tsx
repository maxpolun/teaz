import * as React from 'react'
import TeaList from '@src/systems/teas/TeaList'
import { Link, Switch, Route } from 'react-router-dom'
import NewTeaForm from '@src/systems/teas/NewTeaForm'
import { connect } from 'react-redux'
import { ensureTeaCategoriesLoaded } from '../systems/teaCategories/teaCategories'

interface TeaPageProps {
  ensureTeaCategoriesLoaded: typeof ensureTeaCategoriesLoaded
}

export class TeaPage extends React.Component<TeaPageProps> {
  componentDidMount () {
    this.props.ensureTeaCategoriesLoaded()
  }
  render () {
    return <div>
      <Switch>
        <Route path='/teas' exact render={() => <React.Fragment>
          <h2>Your Teas <Link to='/teas/new' className='btn'>Add Tea</Link></h2>
          <TeaList />
        </React.Fragment>} />
        <Route path='/teas/new' render={() => <React.Fragment>
          <h2>Add new tea</h2>
          <NewTeaForm />
        </React.Fragment>} />
      </Switch>
    </div>
  }
}

export default connect(state => ({}), { ensureTeaCategoriesLoaded })(TeaPage)
