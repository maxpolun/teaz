import * as React from 'react'
import Checkauth from '../systems/auth/Checkauth.container'
import { Switch, Route, Redirect } from 'react-router'
import Login from '@src/systems/auth/Login.container'
import Register from '@src/systems/auth/Register.container'
import TeaList from '@src/systems/teas/TeaList'
import NavMenu from '@src/shared/components/NavMenu'
import NavLayout from '@src/systems/navigation/NavLayout.container'
import TeaPage from './TeaPage'

export default class LandingPage extends React.Component<{}> {
  render () {
    return <Checkauth
      unauthorized={<NavLayout><Switch>
        <Route path='/login' component={Login} />
        <Route path='/register' component={Register} />
        <Redirect to='/login' />
      </Switch></NavLayout>}
      authorized={<NavLayout nav={<NavMenu/>}><Switch>
        <Route path='/teas' component={TeaPage} />
        <Redirect to='/teas' />
      </Switch></NavLayout>}
    />
  }
}
