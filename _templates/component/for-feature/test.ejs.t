---
to: js/systems/<%= feature %>/<%= name %>.test.tsx
---
import <%= name %> from './<%= name %>'
import * as React from 'react'
import {shallow} from 'enzyme'

describe('<%= name %>', () => {
  function render () {
    return shallow(<<%= name %> />)
  }
  it('can render', () => {
    expect(render()).toExist()
  })
})
