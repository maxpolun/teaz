---
to: js/systems/<%= locals.feature %>/<%= name %>/<%= name %>.ts
inject: true
after: hygen:actionTypes
---
<%
  ACTION_TYPE = h.inflection.underscore(actionName).toUpperCase()
-%>
export const <%= ACTION_TYPE %> = 'teaz/<%= name %>/<%= actionName %>'
