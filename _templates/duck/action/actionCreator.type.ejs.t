---
to: js/systems/<%= locals.feature %>/<%= name %>/<%= name %>.ts
inject: true
after: type <%= h.inflection.camelize(name) %>Action
---
  | <%= h.inflection.camelize(actionName) %>
