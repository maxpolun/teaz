---
to: js/systems/<%= locals.feature %>/<%= name %>/<%= name %>.ts
inject: true
after: hygen:actionCreators
---
<%
  ACTION_TYPE = h.inflection.underscore(actionName).toUpperCase()
  ActionType = h.inflection.camelize(actionName)
-%>
export interface <%= ActionType %> {
  type: typeof <%= ACTION_TYPE %>
}
export const <%= actionName %> = (): <%= ActionType %> => ({type: <%= ACTION_TYPE %>})
