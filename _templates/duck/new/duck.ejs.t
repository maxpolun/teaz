---
to: js/systems/<%= locals.feature %>/<%= name %>/<%= name %>.duck.ts
---
import { AllActions, RootState } from '@src/reduxTypes'
import { AnyAction } from 'redux'

export interface <%= h.inflection.camelize(name) %>State {

}

const defaultState: <%= h.inflection.camelize(name) %>State = {}

// hygen:actionTypes
// hygen:actionCreators

export type <%= h.inflection.camelize(name) %>Action = AnyAction

export function <%= name %>Reducer (state: <%= h.inflection.camelize(name) %>State = defaultState, action: AllActions): <%= h.inflection.camelize(name) %>State {
  switch (action.type) {
    default: return state
  }
}

export const <%= name %>Selector = (state: RootState) => state.<%= name %>
