---
to: js/reduxTypes.ts
inject: true
after: import
skip_if: ./systems/<%= locals.feature %>/<%= name %>/<%= name %>
---
import { <%= h.inflection.camelize(name) %>State, <%= h.inflection.camelize(name) %>Action } from './systems/<%= locals.feature %>/<%= name %>/<%= name %>'<% -%>
