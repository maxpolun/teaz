---
to: js/reducer.ts
inject: true
after: import
skip_if: ./systems/<%= locals.feature %>/<%= name %>/<%= name %>
---
import { <%= name %>Reducer as <%= name %> } from './systems/<%= locals.feature %>/<%= name %>/<%= name %>'<% -%>
