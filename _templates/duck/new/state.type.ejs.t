---
to: js/reduxTypes.ts
inject: true
after: export interface RootState
---
  <%= name %>: <%= h.inflection.camelize(name) %>State<% -%>
