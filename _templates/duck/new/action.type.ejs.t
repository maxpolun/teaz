---
to: js/reduxTypes.ts
inject: true
after: export type AllActions =
---
  | <%= h.inflection.camelize(name) %>Action<% -%>
