---
to: js/systems/<%= locals.feature %>/<%= name %>.container.tsx
---
import * as React from 'react'
import {connect} from 'react-redux'

interface <%= name %>Props {

}

export const <%= name %>: React.SFC<<%= name %>Props> = (props) => {
  return <div>replace me!</div>
}

export default connect(state => state, {})(<%= name %>)
