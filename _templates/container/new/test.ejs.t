---
to: js/systems/<%= locals.feature %>/<%= name %>.container.test.tsx
---
import * as React from 'react'
import {<%= name %>} from './<%= name %>.container'
import {shallow} from 'enzyme'

describe('<%= name %>', () => {
  function render () {
    return shallow(<<%= name %> />)
  }
  it('can render', () => {
    expect(render()).toExist()
  })
})
