const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

const env = process.env.NODE_ENV || 'development'
const isProd = env === 'production'

module.exports = ({ devServer }) => ({
  mode: isProd ? 'production' : 'development',
  context: __dirname,
  target: 'web',
  devtool: isProd ? 'sourcemap' : 'cheap-module-eval-source-map',
  entry: {
    main: './js/main.tsx'
  },
  output: {
    filename: isProd ? '[name]-[chunkhash].js' : '[name].js',
    path: path.join(__dirname, 'build/'),
    publicPath: devServer ? 'http://localhost:8008/' : '/assets/',
    pathinfo: !isProd
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      '@src': path.join(__dirname, 'js')
    }
  },
  devServer: {
    port: 8008,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    overlay: {
      warnings: true,
      errors: true
    }
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'ts-loader',
          options: {
            transpileOnly: true
          }
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.s?css/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      alwaysWriteToDisk: true,
      filename: 'index.html',
      title: 'Teaz',
      inject: false,
      template: require('html-webpack-template'),
      appMountId: 'main'
    }),
    new HtmlWebpackHarddiskPlugin(),
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development',
      WEBPACK_SERVE: false
    }),
    new ForkTsCheckerWebpackPlugin()
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /node_modules/,
          chunks: 'initial',
          name: 'vendor',
          enforce: true
        }
      }
    }
  }
})
