-- Your SQL goes here

CREATE TABLE users (
  id VARCHAR(21) PRIMARY KEY,
  email TEXT NOT NULL UNIQUE
);
