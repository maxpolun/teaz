-- Your SQL goes here

CREATE TABLE tea_categories (
  id SERIAL PRIMARY KEY,
  name TEXT NOT NULL
);

INSERT INTO tea_categories (name)
  VALUES
  ('green'),
  ('black'),
  ('white'),
  ('oolong'),
  ('puer'),
  ('heicha'),
  ('yellow'),
  ('herbal'),
  ('other')
;

CREATE TABLE teas (
  id VARCHAR(21) PRIMARY KEY,
  category_id INTEGER NOT NULL REFERENCES tea_categories(id),
  user_id VARCHAR(21) NOT NULL REFERENCES users(id),
  subcategory TEXT,
  name TEXT NOT NULL,
  description TEXT,
  rating SMALLINT,
  url TEXT
);
