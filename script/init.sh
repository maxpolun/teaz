#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

# check for diesel command
hash diesel 2>/dev/null || { echo >&2 "diesel_cli is not installed, please run `cargo install diesel_cli`"; exit 1; }

npm ci
createuser -s teaz
diesel database setup
