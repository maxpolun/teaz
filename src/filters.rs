use warp::Filter;
use diesel::PgConnection;
use http::response::{Response};
use http::header::{COOKIE, SET_COOKIE, HeaderMap};
use http::status::StatusCode;
use cookie::{Cookie, CookieJar, Key};
use serde::Serialize;
use std::env;
use::log::{error, debug};
use hyper::Body;
use diesel::r2d2::ConnectionManager;
use r2d2::Pool;
use lazy_static::lazy_static;

lazy_static! {
  static ref POOL: Pool<ConnectionManager<PgConnection>> = {
    let database_url = env::var("DATABASE_URL")
      .expect("DATABASE_URL must be set");
    let manager = ConnectionManager::<PgConnection>::new(database_url);
    Pool::builder().build(manager).expect("Failed to create pool.")
  };
}

pub struct ApiCtx {
  pub pool: Pool<ConnectionManager<PgConnection>>,
  pub cookies: CookieJar,
  pub cookie_key: Key,
}

impl ApiCtx {
  pub fn conn(&self) -> r2d2::PooledConnection<ConnectionManager<PgConnection>> {
    self.pool.get().expect("error connecting to database")
  }
}

fn cookie_signing_key() -> Key {
  Key::from_master(env::var("COOKIE_KEY").expect("COOKIE_KEY must be set").as_bytes())
}

pub fn api_ctx() -> warp::filters::BoxedFilter<(ApiCtx,)> {
  warp::filters::header::headers_cloned().map(|headers: HeaderMap| {
    let pool = POOL.clone();
    let mut cookies = CookieJar::new();
    let key = cookie_signing_key();
    let mut signed_cookies = cookies.signed(&key);
    for raw_cookie in headers.get(COOKIE) {
      if let Ok(cookie) = Cookie::parse(raw_cookie.to_str().expect("invalid cookie header text")) {
        signed_cookies.add_original(cookie.into_owned())
      }
    }
    ApiCtx {
      pool, cookies, cookie_key: key
    }
  }).boxed()
}

pub fn api_response_with_status<T: Serialize>(ctx: ApiCtx, data: &T, status: StatusCode) -> Result<http::Response<Body>, warp::Rejection> {
  let mut builder = Response::builder();
  builder.status(status);
  for cookie in ctx.cookies.delta() {
    debug!("setting cookie: {}", cookie);
    builder.header(SET_COOKIE, format!("{}", cookie).as_str());
  }
  let body = serde_json::to_vec(data).map_err(|_| {
    error!("error serializing json");
    warp::reject::server_error()
  })?;
  Ok(builder.body(body.into()).map_err(|_| warp::reject::server_error())?)
}

pub fn api_response<T: Serialize>(ctx: ApiCtx, data: &T) -> Result<http::Response<Body>, warp::Rejection> {
  api_response_with_status(ctx, data, StatusCode::OK)
}
