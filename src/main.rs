#[macro_use] extern crate diesel;
#[macro_use] extern crate failure;
#[macro_use] extern crate serde_derive;

use warp::{Filter, path};
use self::modules::users::user_routes;
use self::modules::teas::tea_routes;
use self::modules::auth::auth_routes;
use dotenv::dotenv;
use std::env;

pub mod modules;
pub mod schema;
pub mod filters;
pub mod util;

fn main() {
    dotenv().ok();
    if env::var_os("RUST_LOG").is_none() {
        // Set `RUST_LOG=teaz=debug` to see debug logs,
        // this only shows access logs.
        env::set_var("RUST_LOG", "teaz=debug");
    }
    pretty_env_logger::init();

    let api = warp::path("api").and(
        auth_routes()
        .or(user_routes())
        .or(tea_routes())
        );

    let render_index = warp::get2().and(warp::fs::file("./build/index.html"));

    let root = render_index.clone()
                    .and(warp::index());
    let assets = warp::path("assets")
                    .and(warp::fs::dir("./build/"));

    let login = render_index.clone()
                    .and(path!("login"));
    let register = render_index.clone()
                    .and(path!("register"));

    let front_end = login.or(register).or(render_index.clone().and(path!("teas")));
    let routes = root.or(assets).or(api).or(front_end).with(warp::log("teaz"));
    warp::serve(routes)
        .run(([127, 0, 0, 1], env::var("PORT").map(|port| port.parse::<u16>().unwrap_or(3030)).unwrap_or(3030)));
}
