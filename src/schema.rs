table! {
    tea_categories (id) {
        id -> Int4,
        name -> Text,
    }
}

table! {
    teas (id) {
        id -> Varchar,
        category_id -> Int4,
        user_id -> Varchar,
        subcategory -> Nullable<Text>,
        name -> Text,
        description -> Nullable<Text>,
        rating -> Nullable<Int2>,
        url -> Nullable<Text>,
    }
}

table! {
    user_sessions (id) {
        id -> Text,
        user_id -> Varchar,
        created_at -> Timestamptz,
    }
}

table! {
    users (id) {
        id -> Varchar,
        email -> Text,
        pw_hash -> Text,
    }
}

joinable!(teas -> tea_categories (category_id));
joinable!(teas -> users (user_id));
joinable!(user_sessions -> users (user_id));

allow_tables_to_appear_in_same_query!(
    tea_categories,
    teas,
    user_sessions,
    users,
);
