use crate::filters::{ApiCtx, api_response, api_response_with_status};
use crate::modules::users::{User, get_user_by_id, get_user_by_email, create_user, CreateUserError};
use super::session_model::{get_session, create_session, delete_session};
use warp::reject::{forbidden, server_error};
use http::status::StatusCode;
use cookie::{Cookie};
use log::{debug, trace};

fn auth_cookie_name() -> String {
  let env = std::env::var("TEAZ_ENV").expect("TEAZ_ENV variable required");
  format!("teaz_{}_sessionid", env)
}

// validate that the user is authorized
pub fn auth(mut ctx: ApiCtx) -> Result<(ApiCtx, User), warp::Rejection> {
  let cookie = ctx.cookies.signed(&ctx.cookie_key).get(&auth_cookie_name()).ok_or_else(|| {
    debug!("didn't get auth cookie");
    forbidden()
  })?;
  // split the cookie manually -- I think the cookie library should do this, and it looks like there's code to, but it doesn't seem to be doing it.
  let session_id = cookie.value().split("=").nth(1).ok_or(forbidden())?;
  trace!("got valid session cookie with id: {}", session_id);
  let conn = ctx.conn();
  let session = get_session(&conn, session_id).map_err(|_| forbidden())?;
  let user = get_user_by_id(&conn, &session.user_id).ok_or(forbidden())?;
  debug!("logged in as user: {}", user.id);
  Ok((ctx, user))
}

#[derive(Deserialize)]
pub struct LoginRequestBody {
  email: String,
  password: String
}

#[derive(Serialize)]
pub struct LoginResponseBody {
  user_id: String
}

pub fn login(mut ctx: ApiCtx, body: LoginRequestBody) -> Result<impl warp::Reply, warp::Rejection> {
  let conn = ctx.conn();
  let user = get_user_by_email(&conn, &body.email).map_err(|_| forbidden())?;
  if !user.validate_password(&body.password) {
    return Err(forbidden());
  }
  let session = create_session(&conn, &user.id).map_err(|_| server_error())?;
  ctx.cookies.signed(&ctx.cookie_key).add(
    Cookie::build(auth_cookie_name(), session.id.clone())
      .http_only(true)
      .permanent()
      .finish()
  );
  debug!("creating auth cookie for session {}: {:?}", &session.id, ctx.cookies.signed(&ctx.cookie_key).get(&auth_cookie_name()));
  let response_body = LoginResponseBody {
    user_id: user.id.clone()
  };
  api_response(ctx, &response_body)
}

#[derive(Serialize)]
struct LogoutResponseBody {}

pub fn logout(mut ctx: ApiCtx) -> Result<impl warp::Reply, warp::Rejection> {
  let conn = ctx.conn();
  let cookie = ctx.cookies.signed(&ctx.cookie_key).get(&auth_cookie_name()).ok_or(server_error())?;
  let session_id = cookie.value();
  if !delete_session(&conn, session_id) {
    return Err(server_error());
  }
  ctx.cookies.signed(&ctx.cookie_key).remove(cookie);
  api_response(ctx, &LogoutResponseBody{})
}

#[derive(Deserialize)]
pub struct RegisterRequestBody {
  email: String,
  password: String,
  password_confirmation: String
}

#[derive(Serialize)]
pub struct RegisterError {
  error: RegisterErrorType
}

#[derive(Serialize)]
#[serde(rename_all = "snake_case")]
pub enum RegisterErrorType {
  InvalidPassword,
  PasswordConfirmationNoMatch,
  InvalidEmail,
  UserExists
}

#[derive(Serialize)]
#[serde(untagged)]
pub enum RegisterResponseBody {
  Success(User),
  Error(RegisterError)
}

pub fn register(ctx: ApiCtx, body: RegisterRequestBody) -> Result<impl warp::Reply, warp::Rejection> {
  debug!("trying to register");
  if body.password != body.password_confirmation {
    debug!("password and confirmation missmatch");
    return api_response_with_status(ctx, &RegisterResponseBody::Error(RegisterError{ error: RegisterErrorType::PasswordConfirmationNoMatch }), StatusCode::BAD_REQUEST);
  }

  let conn = ctx.conn();
  let user = create_user(&conn, &body.email, &body.password);
  debug!("created user {:?}", &user);
  match user {
    Ok(user) => api_response(ctx, &RegisterResponseBody::Success(user)),
    Err(CreateUserError::EmailAlreadyExists { email: _ }) => api_response_with_status(ctx, &RegisterResponseBody::Error(RegisterError { error: RegisterErrorType::UserExists }), StatusCode::BAD_REQUEST),
    Err(CreateUserError::InvalidEmail) => api_response_with_status(ctx, &RegisterResponseBody::Error(RegisterError { error: RegisterErrorType::InvalidEmail }), StatusCode::BAD_REQUEST),
    Err(CreateUserError::BadPassword) => api_response_with_status(ctx, &RegisterResponseBody::Error(RegisterError { error: RegisterErrorType::InvalidPassword }), StatusCode::BAD_REQUEST),
    Err(_) => Err(warp::reject::server_error()),
  }
}
