use warp::Filter;
use warp::filters::BoxedFilter;
use warp::path;
use crate::filters::api_ctx;
use super::{login, logout, register};

pub fn auth_routes() -> BoxedFilter<(impl warp::Reply,)> {
  let login_route = warp::post2()
    .and(path!("login"))
    .and(warp::filters::path::index())
    .and(api_ctx())
    .and(warp::body::json())
    .and_then(login);
  let logout_route = warp::post2()
    .and(path!("logout"))
    .and(warp::filters::path::index())
    .and(api_ctx())
    .and_then(logout);
  let register_route = warp::post2()
    .and(path!("register"))
    .and(warp::filters::path::index())
    .and(api_ctx())
    .and(warp::body::json())
    .and_then(register);

  login_route.or(logout_route).or(register_route).boxed()
}
