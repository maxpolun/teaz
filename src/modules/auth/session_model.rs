use diesel::PgConnection;
use diesel::prelude::*;
use crate::schema::user_sessions;
use chrono::prelude::*;

#[derive(Queryable, Serialize)]
pub struct Session {
  pub id: String,
  pub user_id: String,
  pub created_at: DateTime<Utc>,
}

#[derive(Insertable)]
#[table_name="user_sessions"]
struct NewSession<'a> {
  id: &'a str,
  user_id: &'a str,
}

pub fn create_session(conn: &PgConnection, user_id: &str) -> Result<Session, diesel::result::Error> {
  let id = nanoid::simple();
  let new_session = NewSession {
    id: &id,
    user_id
  };
  diesel::insert_into(user_sessions::table).values(new_session).get_result(conn)
}

pub fn get_session(conn: &PgConnection, session_id: &str) -> Result<Session, diesel::result::Error> {
  user_sessions::table.find(session_id).first(conn)
}

pub fn delete_session(conn: &PgConnection, session_id: &str) -> bool {
  diesel::delete(user_sessions::table.filter(user_sessions::id.eq(session_id))).execute(conn).is_ok()
}
