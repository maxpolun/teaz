#![allow(proc_macro_derive_resolution_fallback)]

use diesel::pg::PgConnection;
use diesel::prelude::*;
use diesel::result::{Error, DatabaseErrorKind};
use scrypt::{ScryptParams, scrypt_simple, scrypt_check};
use crate::schema::users;

#[derive(Debug, Queryable, Serialize)]
pub struct User {
    pub id: String,
    pub email: String,
    #[serde(skip_serializing)]
    pub pw_hash: String
}

impl User {
  pub fn validate_password (&self, password: &str) -> bool {
    scrypt_check(password, &self.pw_hash).is_ok()
  }
}

pub fn get_user_by_id (conn: &PgConnection, id: &str) -> Option<User> {
  users::table.find(id).first(conn).optional().expect("unknown error querying users")
}

pub fn get_user_by_email (conn: &PgConnection, email: &str) -> Result<User, Error> {
  users::table.filter(users::email.eq(email)).first(conn)
}

#[derive(Debug, Insertable)]
#[table_name="users"]
pub struct NewUser<'a> {
    pub id: &'a str,
    pub email: &'a str,
    pub pw_hash: &'a str
}

#[derive(Debug, Fail)]
pub enum CreateUserError {
  #[fail(display = "invalid email")]
  InvalidEmail,
  #[fail(display = "bad password")]
  BadPassword,
  #[fail(display = "email already exists")]
  EmailAlreadyExists {
    email: String
  },
  #[fail(display = "unexpected error creating user")]
  UnexpectedError
}

fn script_params() -> ScryptParams {
  ScryptParams::new(15, 8, 1).expect("error with script params")
}

pub fn create_user(conn: &PgConnection, email: &str, password: &str) -> Result<User, CreateUserError> {
  if !email.contains("@") {
    return Err(CreateUserError::InvalidEmail)
  }

  if password.len() < 8 {
    return Err(CreateUserError::BadPassword)
  }

  let pw_hash = scrypt_simple(password, &script_params()).map_err(|_| CreateUserError::BadPassword)?;
  let id = nanoid::simple();
  let u = NewUser {
    id: &id,
    email,
    pw_hash: &pw_hash
  };

  diesel::insert_into(users::table)
    .values(&u)
    .get_result(conn)
    .map_err(|err| match err {
      Error::DatabaseError(DatabaseErrorKind::UniqueViolation, _) => CreateUserError::EmailAlreadyExists{ email: email.to_string() },
      _ => CreateUserError::UnexpectedError
    })
}
