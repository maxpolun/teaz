use super::{User};
use warp::Filter;
use warp::filters::BoxedFilter;
use warp::path;
use crate::filters::{api_ctx, ApiCtx, api_response};
use crate::modules::auth::auth;

fn current_user_filter(
  (ctx, user): (ApiCtx, User)
) -> Result<impl warp::Reply, warp::Rejection> {
  api_response(ctx, &user)
}

pub fn user_routes() -> BoxedFilter<(impl warp::Reply,)> {
  let show_user_route = warp::get2()
    .and(path!("users" / "me"))
    .and(warp::filters::path::index())
    .and(api_ctx())
    .and_then(auth)
    .and_then(current_user_filter);

  show_user_route.boxed()
}
