#![allow(proc_macro_derive_resolution_fallback)]
use diesel::pg::PgConnection;
use diesel::prelude::*;
use diesel::result::{Error};
use crate::schema::{tea_categories, teas};

#[derive(Debug, Clone, Queryable, Serialize)]
pub struct TeaCategory {
  pub id: i32,
  pub name: String
}

pub fn get_tea_categories(conn: &PgConnection) -> Vec<TeaCategory> {
  tea_categories::table.load::<TeaCategory>(conn).expect("unexpected error loading tea categories")
}

#[derive(Debug, Clone, Queryable, Serialize)]
pub struct TeaRecord {
  pub id: String,
  #[serde(skip_serializing)]
  pub category_id: i32,
  #[serde(skip_serializing)]
  pub user_id: String,
  pub subcategory: Option<String>,
  pub name: String,
  pub description: Option<String>,
  pub rating: Option<i16>,
  pub url: Option<String>,
}

fn get_all_tea_records_for_user (conn: &PgConnection, user_id: &str) -> Result<Vec<TeaRecord>, Error> {
  teas::table
    .filter(teas::user_id.eq(user_id))
    .load::<TeaRecord>(conn)
}

pub fn get_all_teas_for_user (conn: &PgConnection, user_id: &str) -> Result<Vec<Tea>, Error> {
  let tea_categories = get_tea_categories(conn);
  let tea_records = get_all_tea_records_for_user(conn, user_id)?;
  let default_category = &tea_categories[0];
  Ok(tea_records.iter().map(|record| {
    let category = tea_categories.iter().find(|&cat| cat.id == record.category_id).unwrap_or(default_category);
    Tea {
      record: (*record).clone(),
      category: (*category).clone()
    }
  }).collect::<Vec<_>>())
}

#[derive(Debug, Serialize)]
pub struct Tea {
  #[serde(flatten)]
  record: TeaRecord,
  category: TeaCategory
}

#[derive(Debug, Insertable)]
#[table_name="teas"]
pub struct NewTea<'a> {
  pub id: String,
  pub category_id: i32,
  pub user_id: &'a str,
  pub subcategory: Option<String>,
  pub name: String,
  pub description: Option<String>,
  pub rating: Option<i16>,
  pub url: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct NewTeaRequest {
  pub category_id: i32,
  pub subcategory: Option<String>,
  pub name: String,
  pub description: Option<String>,
  pub rating: Option<i16>,
  pub url: Option<String>,
}

fn create_tea_record(conn: &PgConnection, user_id: &str, req: NewTeaRequest) -> Result<TeaRecord, Error> {
  let id = nanoid::simple();
  let new_tea = NewTea {
    id,
    category_id: req.category_id,
    user_id: user_id,
    name: req.name,
    subcategory: req.subcategory,
    description: req.description,
    rating: req.rating,
    url: req.url
  };
  diesel::insert_into(teas::table)
    .values(&new_tea)
    .get_result(conn)
}

pub fn create_tea(conn: &PgConnection, user_id: &str, req: NewTeaRequest) -> Result<Tea, Error> {
  let record = create_tea_record(conn, user_id, req)?;
  let tea_categories = get_tea_categories(conn);
  let category = tea_categories.iter().find(|tc| tc.id == record.category_id).expect("tea has a category that doesn't exist!");
  Ok(Tea {
    record,
    category: (*category).clone()
  })
}
