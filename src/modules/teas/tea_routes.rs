use super::{get_all_teas_for_user, get_tea_categories, NewTeaRequest, create_tea};
use warp::Filter;
use warp::filters::{BoxedFilter};
use warp::path;
use crate::filters::{api_ctx, ApiCtx, api_response};
use crate::modules::auth::auth;
use crate::modules::users::User;

fn get_categories_filter(ctx: ApiCtx) -> Result<impl warp::Reply, warp::Rejection> {
  let categories = get_tea_categories(&ctx.conn());
  api_response(ctx, &categories)
}

fn get_teas_for_user_filter((ctx, current_user): (ApiCtx, User), user_id: String) -> Result<impl warp::Reply, warp::Rejection> {
  let id = if user_id == "me" { &current_user.id } else { &user_id };
  if id != &current_user.id {
    return Err(warp::reject::forbidden());
  }
  let teas = get_all_teas_for_user(&ctx.conn(), id).map_err(|_| warp::reject::server_error())?;
  api_response(ctx, &teas)
}

fn create_tea_filter((ctx, current_user): (ApiCtx, User), user_id: String, body: NewTeaRequest) -> Result<impl warp::Reply, warp::Rejection> {
  let id = if user_id == "me" { &current_user.id } else { &user_id };
  if id != &current_user.id {
    return Err(warp::reject::forbidden());
  }
  let new_tea = create_tea(&ctx.conn(), &id, body).map_err(|_| warp::reject::server_error())?;
  api_response(ctx, &new_tea)
}

pub fn tea_routes() -> BoxedFilter<(impl warp::Reply,)> {
  let get_teas_route = warp::get2()
    .and(api_ctx())
    .and_then(|ctx| auth(ctx))
    .and(path!("users" / String / "teas"))
    .and(warp::filters::path::index())
    .and_then(get_teas_for_user_filter);
  let get_categories_route = path!("tea-categories")
    .and(warp::filters::path::index())
    .and(api_ctx())
    .and_then(get_categories_filter);
  let create_tea_route = warp::post2()
    .and(api_ctx())
    .and_then(|ctx| auth(ctx))
    .and(path!("users" / String / "teas"))
    .and(warp::filters::path::index())
    .and(warp::body::json())
    .and_then(create_tea_filter);

  get_teas_route.or(get_categories_route).or(create_tea_route).boxed()
}
